#include <stdio.h>
#include <stdlib.h>
#include "linked_list.h"

void init_empty()
{
  node_t *empty = NULL;

  empty = (node_t *)malloc(sizeof(node_t*));

  empty.data = NULL;
  empty->next = NULL;
}
