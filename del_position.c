#include <stdio.h>
#include <stdlib.h>
#include "linked_list.h"

void del_position(node_t *head_ref, int key)
{
  node_t *temp = *head_ref, *prev;

  if(temp != NULL && temp->data == key) {
    *head_ref = temp->next;
    free(temp);
    return;
  }
  
  while(temp != NULL && temp->data != key) {
    prev = temp;
    temp = temp->next;
  }
  
  if(temp == NULL) {
    return;
  }
    
  prev->next = temp->next;

  free(temp);
}
