#include <stdio.h>
#include <stdlib.h>
#include "LL_create_node.h"

int create_node()
{
  node_t *head = NULL;
  node_t *second = NULL;
  node_t *third = NULL;

  head = (node_t *)malloc(sizeof(node_t));
  second = (node_t *)malloc(sizeof(node_t));
  third = (node_t *)malloc(sizeof(node_t));

  head->data = 1;
  head->next = second;

  second->data = 2;
  second->next = third;

  third->data = 3;
  third->next = NULL;

  return 0;
}
