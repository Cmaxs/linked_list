#include <stdio.h>
#include <stdlib.h>
#include "linked_list.h"

void insert_behind(node_t *prev_n, int new_data)
{
  if(prev_n == NULL) {
    printf("Cannot be null\n");
    return;
  }
  
  node_t *new_node = (node_t *)malloc(sizeof(node_t *));
  
  new_node->data = new_data;

  new_node->next = prev_n->next;

  prev_n->next = new_node;

  return;
}

