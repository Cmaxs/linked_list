#include <stdio.h>
#include <stdlib.h>
#include "linked_list_"

void deleting(node_t *head_ptr, int mark)
{
  node_t *temp = *head_ptr, *prev_n;

  //case that head node is the one to be deleted - mark matches
  if(temp != NULL && temp->data == mark) {
    *head_ptr = temp->next;
    free(temp);
    return;
  }

  while(temp != NULL && temp->data != mark) {
    prev = temp;
    temp = temp->next;
  }
  
  if(temp == NULL) {
    return;
  }

  prev->next = temp->next;

  free(temp);
}
