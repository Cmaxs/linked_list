#ifndef linked_list_
#define linked_list_

typedef struct Node {
        int data;
        struct Node *next;
} node_t;

void create_node();
void insert_node(node_t *prev_n, int new_data);
void del_position(node_t *head_ref, int key);
void deleting(node_t *head_ptr, int mark);

void init_empty();
void del_all(node_t *head);

#endif

