#include <stdio.h>
#include <stdlib.h>
#include "linked_list.h"

void del_all(node_t *head)
{
  node_t *prev = *head;

  while(*head) {
    *head = *head->next;

    free(prev);
    prev = *head;
  } 
  
  return;
}
